Pasos para ejecutar el proyecto:
1- Restaurar el backup adjuntando en la raíz del proyecto con el nombre "RentalMovies.bak"
    -Abrir la instancia de la localdb en el sql server
    -click derecho en la opción  "databases" en el sql server, seleccionar "restore database..."
    -En la nueva ventana seleccionar "Device" en el apartado source, click en el botón de los tres puntos "..."
    -Se abrirá nueva ventana, click en el botón "add" y buscar el archivo a restaurar, en este caso "RentalMovies.bak"
    -Click en "Ok" y luego nuevamente en "Ok" y la base de datos será restaurada con datos de prueba ya ingresados

2- Ejecutar visual studio y correr el proyecto o levantar la API desde el ejecutable de la aplicación en la carpeta Bin.

3- Para fácilitar las pruebas, utilizar el archivo de postman collection adjuntado en la raíz del proyecto.


    
    
