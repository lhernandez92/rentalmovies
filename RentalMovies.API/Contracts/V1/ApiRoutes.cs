﻿namespace RentalMovies.API.Contracts.V1
{
    public class ApiRoutes
    {
        private const string Root = "api";
        private const string Version = "v1";
        public const string Base = Root + "/" + Version + "/[controller]";
    }
}
