﻿using Microsoft.AspNetCore.Mvc;
using RentalMovies.API.Contracts.V1;
using RentalMovies.Application.Services.Interfaces;
using RentalMovies.Application.V1.Request;
using RentalMovies.Application.V1.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentalMovies.API.Controllers.V1
{
    [Route(ApiRoutes.Base)]
    public class IdentityController : ControllerBase
    {
        private readonly IIdentityService _identityService;

        public IdentityController(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegistrationRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new AuthenticationResponse
                {
                    Error = ModelState.Values.SelectMany(e => e.Errors.Select(x => x.ErrorMessage)).SingleOrDefault()
                });
            }

            var authResponse = await _identityService.RegisterAsync(request);

            if (authResponse.Success)
                return Ok(authResponse);

            return BadRequest(authResponse);

        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new AuthenticationResponse
                {
                    Error = ModelState.Values.SelectMany(e => e.Errors.Select(x => x.ErrorMessage)).SingleOrDefault()
                });
            }

            var authResponse = await _identityService.LoginAsync(request);

            if (authResponse.Success)
                return Ok(authResponse);

            return BadRequest(authResponse);
        }

        [HttpPost("refresh")]
        public async Task<IActionResult> Refresh([FromBody] RefreshTokenRequest request)
        {
            var authResponse = await _identityService.RefreshTokenAsyncAsync(request);

            if (authResponse.Success)
                return Ok(authResponse);

            return BadRequest(authResponse);
        }
    }
}
