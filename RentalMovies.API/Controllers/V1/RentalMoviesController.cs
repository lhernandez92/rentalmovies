﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;
using RentalMovies.API.Contracts.V1;
using RentalMovies.Application.Services.Interfaces;
using RentalMovies.Application.V1.Request;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RentalMovies.API.Controllers.V1
{
    [Route(ApiRoutes.Base)]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RentalMoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;
        public RentalMoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet]
        [Route("{page}/{available}/{title?}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAll([FromRoute] int page = 1, [FromRoute] bool available = true, [FromRoute] string title = null)
        {
            return Ok( await _movieService.GetMoviesAsync(User, page, title, available) );
        }

        [HttpGet("{idMovie}")]
        [AllowAnonymous]
        public async Task<IActionResult> Get([FromRoute] int idMovie)
        {
            return Ok( await _movieService.GetMovieByIdAsync(idMovie) ); 
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([FromBody] AddMovieRequest request)
        {
            var result = await _movieService.CreateMovieAsync(request);

            if (result == 0)
                return BadRequest(new { Error = "Sorry, there was a problem adding the movie. Please try again" });

            return Ok(new { IdMovie = result });            
        }

        [HttpPut]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Update([FromBody] UpdateMovieRequest request)
        {
            var result = await _movieService.UpdateMovieAsync(request);;

            if (result)
                return NoContent();

            return BadRequest(new { Error = "Sorry, there was a problem updating the movie. Please try again" });
        }

        [HttpDelete("{idMovie}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete([FromRoute] int idMovie)
        {
            var result = await _movieService.DeleteMovieAsync(idMovie); ;

            if (result)
                return NoContent();

            return BadRequest(new { Error = "Sorry, there was a problem deleting the movie. Please try again" });
        }

        [HttpPost("Rental")]
        [Authorize(Roles = "Admin,User")]
        public async Task<IActionResult> RentalMovie([FromBody] RentalRequest request)
        {
            var result = await _movieService.RentalMovieAsync(request);

            if (result.Success)
                return NoContent();

            return BadRequest(result);
        }

        [HttpPut("Return")]
        [Authorize(Roles = "Admin,User")]
        public async Task<IActionResult> ReturnMovie([FromBody] ReturnMovieRequest request)
        {
            var result = await _movieService.ReturnMovieAsync(request);

            if (result.Success)
                return NoContent();

            return BadRequest(result);
        }

        [HttpDelete("RemoveStock")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> RemoveStock([FromBody] RemoveStockRequest request)
        {
            var result = await _movieService.RemoveStockAsync(request);

            if (result.Success)
                return NoContent();

            return BadRequest(result);
        }

        [HttpPost("Like")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> AddLike([FromBody] LikeRequest request)
        {
            var result = await _movieService.AddLikeAsync(request, User);

            if (result.Success)
                return NoContent();

            return BadRequest(result);
        }
    }
}
