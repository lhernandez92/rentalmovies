﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RentalMovies.Data;
using RentalMovies.API.Installers.Interfaces;
using RentalMovies.Application.Services;
using RentalMovies.Application.Services.Interfaces;

namespace RentalMovies.API.Installers
{
    public class DataInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<RentalMoviesDbContext>(options =>
            options.UseSqlServer(
                configuration.GetConnectionString("RentalMovies"))
            );

            services.AddDefaultIdentity<IdentityUser>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<RentalMoviesDbContext>();

            services.AddScoped<IMovieService, MovieService>();
            services.AddScoped<IIdentityService, IdentityService>();
        }
    }
}
