﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RentalMovies.API.Installers.Interfaces;
using System;
using System.Linq;

namespace RentalMovies.API.Installers
{
    public static class InstallerExtension
    {
        public static void InstallServicesAssembly(this IServiceCollection services, IConfiguration configuration)
        {
            var Installers = typeof(Startup).Assembly.
                            ExportedTypes.Where(x => typeof(IInstaller).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
                            .Select(Activator.CreateInstance).Cast<IInstaller>().ToList();

            Installers.ForEach(installer => installer.InstallServices(services, configuration));
        }
    }
}
