﻿using RentalMovies.Application.V1.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RentalMovies.Application.Helpers
{
    public abstract class PaginationBase
    {
        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
        public int PageSize { get; set; }
        public int TotalMovies { get; set; }

        public int FirstMovieOnPage
        {
            get { return (CurrentPage - 1) * PageSize + 1; }
        }

        public int LastMovieOnPage
        {
            get { return Math.Min(CurrentPage * PageSize, TotalMovies); }
        }
    }

    public class Pagination<MovieResponse> : PaginationBase
    {
        public IList<MovieResponse> Results { get; set; }

        public Pagination()
        {
            Results = new List<MovieResponse>();
        }
    }

    public static class PaginationExtension
    {
        public static Pagination<MovieResponse> GetPaged(this IEnumerable<MovieResponse> query, int page, int pageSize)
        {
            var result = new Pagination<MovieResponse>
            {
                CurrentPage = page,
                PageSize = pageSize,
                TotalMovies = query.Count()
            };

            var pageCount = (double)result.TotalMovies / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;
            result.Results = query.OrderBy(m => m.Title)
                .ThenByDescending(m => m.LikesQuantity)
                .Skip(skip).Take(pageSize).ToList();

            return result;
        }
    }
}
