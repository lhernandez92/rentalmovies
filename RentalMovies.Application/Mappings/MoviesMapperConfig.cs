﻿using AutoMapper;
using RentalMovies.Application.V1;
using RentalMovies.Application.V1.Request;
using RentalMovies.Application.V1.Response;
using RentalMovies.Domain.Entities;
using RentalMovies.Domain.Enum;
using System;
using System.Linq;

namespace RentalMovies.Application.Mappings
{
    public class MoviesMapperConfig : Profile
    {
        public MoviesMapperConfig()
        {
            CreateMap<Movie, MovieResponse>()
                .ForMember(d => d.IdMovie, o => o.MapFrom(e => e.IdMovie))
                .ForMember(d => d.Title, o => o.MapFrom(e => e.Title))
                .ForMember(d => d.Description, o => o.MapFrom(e => e.Description))
                .ForMember(d => d.Image, o => o.MapFrom(e => e.Image))
                .ForMember(d => d.RentalPrice, o => o.MapFrom(e => e.RentalPrice))
                .ForMember(d => d.SalePrice, o => o.MapFrom(e => e.SalePrice))
                .ForMember(d => d.PenaltyMoney, o => o.MapFrom(e => e.PenaltyMoney))
                .ForMember(d => d.IsAvailable, o => o.MapFrom(e => e.Stocks.Where(x => x.IsAvailable).Count() > 0))
                .ForMember(d => d.StocksQuantity, o => o.MapFrom(e => e.Stocks.Where(s => s.IsAvailable).Count()))
                .ForMember(d => d.LikesQuantity, o => o.MapFrom(e => e.Likes.Count));


            CreateMap<MovieRequestBase, Movie>()
                .ForMember(d => d.Title, o => o.MapFrom(e => e.Title))
                .ForMember(d => d.Description, o => o.MapFrom(e => e.Description))
                .ForMember(d => d.Image, o => o.MapFrom(e => e.Image))
                .ForMember(d => d.RentalPrice, o=> o.MapFrom(e => e.RentalPrice))
                .ForMember(d => d.SalePrice, o => o.MapFrom(e => e.SalePrice))
                .ForMember(d => d.PenaltyMoney, o => o.MapFrom(e => e.PenaltyMoney));

            CreateMap<RentalRequest, HistoryTransaction>()
                .ForMember(d => d.IdUser, o => o.MapFrom(e => e.IdUser))
                .ForMember(d => d.RentalDate, o => o.MapFrom(e => DateTime.Now))
                .ForMember(d => d.PenaltyDate, o => o.MapFrom(e => GetRerutnDate(e.Status, e)))
                .ForMember(d => d.Status, o => o.MapFrom(e => e.Status))
                .ForMember(d => d.Payment, o => o.MapFrom(e => e.Payment));
        }

        private DateTime? GetRerutnDate(StatusMovie status, RentalRequest request)
        {
            return status == StatusMovie.Rented 
                ? DateTime.Now.AddDays(request.Days) 
                : (DateTime?)null;
        }
    }
}
