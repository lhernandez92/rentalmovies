﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using RentalMovies.Application.Services.Interfaces;
using RentalMovies.Application.Settings;
using RentalMovies.Application.V1;
using RentalMovies.Application.V1.Request;
using RentalMovies.Application.V1.Response;
using RentalMovies.Data;
using RentalMovies.Domain.Entities;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RentalMovies.Application.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly JwtSetting _jwtSetting;
        private readonly TokenValidationParameters _tokenValidationParamaeters;
        private readonly RentalMoviesDbContext _dbContext;
        private readonly RoleManager<IdentityRole> _roleManager;
        public IdentityService(
            UserManager<IdentityUser> userManager, 
            JwtSetting jwtSetting, 
            TokenValidationParameters tokenValidationParamaeters, 
            RentalMoviesDbContext dbContext,
            RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _jwtSetting = jwtSetting;
            _tokenValidationParamaeters = tokenValidationParamaeters;
            _dbContext = dbContext;
            _roleManager = roleManager;
        }
        public async Task<AuthenticationResponse> LoginAsync(IdentityRequestBase request)
        {
            //TODO: Add SignInManager to handle login and logout
            var user = await _userManager.FindByEmailAsync(request.Email);

            if (user == null)
                return ReturnResponseWithErrorMessage("User does not exist");

            var userValidPassword = await _userManager.CheckPasswordAsync(user, request.Password);

            if (!userValidPassword)
                return ReturnResponseWithErrorMessage("User or password is wrong");

            return await GenerateAuthenticationUsersAsync(user);
        }

        public async Task<AuthenticationResponse> RefreshTokenAsyncAsync(RefreshTokenRequest request)
        {
            var validatedToken = GetPrincipalFromToken(request.Token);

            if (validatedToken == null)
                return ReturnResponseWithErrorMessage("Invalid token");

            var ExpiryDateUnix =
                long.Parse(validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Exp).Value);
            var expiryDateTimeUtc = new DateTime(1970, 1, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                .AddSeconds(ExpiryDateUnix);

            if (expiryDateTimeUtc > DateTime.UtcNow)
                return ReturnResponseWithErrorMessage("This token hasn't expired yet");

            var jti = validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var storedRefreshToken = await _dbContext.RefreshTokens.SingleOrDefaultAsync(x => x.Token == request.RefreshToken);

            if (storedRefreshToken == null)
                return ReturnResponseWithErrorMessage("This refresh token doesn't exist");

            if (DateTime.UtcNow > storedRefreshToken.ExpiryDate)
                return ReturnResponseWithErrorMessage("This refresh token has expired");

            if (storedRefreshToken.Invalidated)
                return ReturnResponseWithErrorMessage("This refresh token has been invalidated");

            if (storedRefreshToken.Used)
                return ReturnResponseWithErrorMessage("This refresh token has been used");

            if (storedRefreshToken.IdJwt != jti)
                return ReturnResponseWithErrorMessage("This refresh token doesn't match this JWT");

            storedRefreshToken.Used = true;
            _dbContext.RefreshTokens.Update(storedRefreshToken);
            await _dbContext.SaveChangesAsync();

            var user = await _userManager.FindByIdAsync(validatedToken.Claims.Single(x => x.Type == "id").Value);
            return await GenerateAuthenticationUsersAsync(user);
        }

        public async Task<AuthenticationResponse> RegisterAsync(IdentityRequestBase request)
        {
            var registrationRequest = (RegistrationRequest)request;            
            var user = await _userManager.FindByEmailAsync(request.Email);
            
            if (user != null)
                return ReturnResponseWithErrorMessage("User with this email address already exist");

            var newUser = new IdentityUser
            {
                Email = request.Email,
                UserName = request.Email
            };

            var createdUser = await _userManager.CreateAsync(newUser, request.Password);
            await _userManager.AddToRoleAsync(newUser, registrationRequest.Role.ToString());

            if (!createdUser.Succeeded)
                return new AuthenticationResponse
                {
                    Error = createdUser.Errors.Select(x => x.Description).SingleOrDefault()
                };

            return await GenerateAuthenticationUsersAsync(newUser);
        }

        private AuthenticationResponse ReturnResponseWithErrorMessage(string message)
        {
            return new AuthenticationResponse { Success = false, Error = message };
        }

        private async Task<AuthenticationResponse> GenerateAuthenticationUsersAsync(IdentityUser user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSetting.Secret);

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim("id", user.Id)
            };

            var userClaims = await _userManager.GetClaimsAsync(user);
            claims.AddRange(userClaims);

            var userRoles = await _userManager.GetRolesAsync(user);
            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
                var role = await _roleManager.FindByNameAsync(userRole);
                if (role == null) continue;
                var roleClaims = await _roleManager.GetClaimsAsync(role);

                foreach (var roleClaim in roleClaims)
                {
                    if (claims.Contains(roleClaim))
                        continue;

                    claims.Add(roleClaim);
                }
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.Add(_jwtSetting.TokenLifetime),
                SigningCredentials =
                new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var refreshToken = new RefreshToken
            {
                IdJwt = token.Id,
                IdUser = user.Id,
                CreationDate = DateTime.UtcNow,
                ExpiryDate = DateTime.UtcNow.AddMonths(6)
            };

            await _dbContext.RefreshTokens.AddAsync(refreshToken);
            var result = await _dbContext.SaveChangesAsync();

            if (result > 0)
            {
                return new AuthenticationResponse
                {
                    Success = true,
                    Token = tokenHandler.WriteToken(token),
                    RefreshToken = refreshToken.Token
                };
            }

            return new AuthenticationResponse
            {
                Success = false,
                Token = null,
                RefreshToken = null
            };
        }

        private ClaimsPrincipal GetPrincipalFromToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                var tokenValidationParameters = _tokenValidationParamaeters.Clone();
                tokenValidationParameters.ValidateLifetime = false;
                var principal = tokenHandler.ValidateToken(token, _tokenValidationParamaeters, out var validatedToken);

                if (!IsJwtWithValidSecurityAlgorithm(validatedToken))
                    return null;

                return principal;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private bool IsJwtWithValidSecurityAlgorithm(SecurityToken validationToken)
        {
            return (validationToken is JwtSecurityToken jwtSecurityToken) &&
                jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
