﻿using RentalMovies.Application.V1;
using RentalMovies.Application.V1.Request;
using RentalMovies.Application.V1.Response;
using System.Threading.Tasks;

namespace RentalMovies.Application.Services.Interfaces
{
    public interface IIdentityService
    {
        Task<AuthenticationResponse> RegisterAsync(IdentityRequestBase request);
        Task<AuthenticationResponse> LoginAsync(IdentityRequestBase request);
        Task<AuthenticationResponse> RefreshTokenAsyncAsync(RefreshTokenRequest request);
    }
}
