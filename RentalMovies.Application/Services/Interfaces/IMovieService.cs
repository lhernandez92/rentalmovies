﻿
using RentalMovies.Application.Helpers;
using RentalMovies.Application.V1;
using RentalMovies.Application.V1.Request;
using RentalMovies.Application.V1.Response;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RentalMovies.Application.Services.Interfaces
{
    public interface IMovieService
    {
        Task<int> CreateMovieAsync(MovieRequestBase movie);
        Task<Pagination<MovieResponse>> GetMoviesAsync(ClaimsPrincipal claims, int page, string name, bool filterAvailable = true);
        Task<MovieResponse> GetMovieByIdAsync(int idMovie);
        Task<bool> UpdateMovieAsync(MovieRequestBase movie);
        Task<bool> DeleteMovieAsync(int idMovie);
        Task<RentalResponse> RentalMovieAsync(RentalRequestBase request);
        Task<RentalResponse> ReturnMovieAsync(RentalRequestBase request);
        Task<RentalResponse> RemoveStockAsync(RentalRequestBase request);
        Task<LikeResponse> AddLikeAsync(LikeRequest request, ClaimsPrincipal claims);
    }
}