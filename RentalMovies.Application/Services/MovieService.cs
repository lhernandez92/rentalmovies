﻿using Microsoft.EntityFrameworkCore;
using RentalMovies.Data;
using RentalMovies.Application.Services.Interfaces;
using RentalMovies.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RentalMovies.Application.V1.Request;
using RentalMovies.Application.V1.Response;
using AutoMapper.QueryableExtensions;
using AutoMapper;
using RentalMovies.Application.V1;
using RentalMovies.Domain.Enum;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Linq.Expressions;
using RentalMovies.Application.Helpers;
using System.Security.Principal;
using Microsoft.Extensions.Logging;

namespace RentalMovies.Application.Services
{
    public class MovieService : IMovieService
    {
        private const int PAGE_SIZE = 4;
        private readonly IMapper _mapper;
        private readonly RentalMoviesDbContext _dbContext;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<MovieService> _logger;
        public MovieService(RentalMoviesDbContext dbContext, IMapper mapper, UserManager<IdentityUser> userManager, ILogger<MovieService> logger)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _userManager = userManager;
            _logger = logger;
        }

        public async Task<int> CreateMovieAsync(MovieRequestBase request)
        {
            var movie = _mapper.Map<Movie>(request);
            await _dbContext.Movies.AddAsync(movie);
            
            var result = await _dbContext.SaveChangesAsync();
            await AddStocksAsync(movie.IdMovie, request.StockQuantity);

            return result > 0 ? movie.IdMovie : 0;
        }

        public async Task<bool> DeleteMovieAsync(int idMovie)
        {
            var movie = await _dbContext.Movies
                .Include(s => s.Stocks)
                .Where(m => m.IdMovie == idMovie)
                .SingleOrDefaultAsync();

            var stock = movie.Stocks.SingleOrDefault(s => s.IsAvailable == false);

            if (stock != null)
                return false;
            
            _dbContext.Movies.Remove(movie);
            var result = await _dbContext.SaveChangesAsync();

            return result > 0;
        }

        public async Task<MovieResponse> GetMovieByIdAsync(int idMovie)
        {
            var result = await _dbContext.Movies
                .Include(s => s.Stocks)
                .Include(l => l.Likes)
                .Where(m => m.IdMovie == idMovie)
                .ProjectTo<MovieResponse>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();

            return result;
        }

        public async Task<Pagination<MovieResponse>> GetMoviesAsync(ClaimsPrincipal claims, int page, string name, bool filterAvailable) 
        {            
            var roleUser = ((ClaimsIdentity)claims.Identity).Claims
                .Where(c => c.Type == ClaimTypes.Role).FirstOrDefault();

            var filter = FilterMovies(roleUser, name, filterAvailable);

            var list = await _dbContext.Movies
                    .Include(s => s.Stocks)
                    .Include(l => l.Likes)
                    .Where(filter)
                    .OrderBy(m => m.Title).ThenByDescending(m => m.Likes.Count)
                    .ProjectTo<MovieResponse>(_mapper.ConfigurationProvider)
                    .ToListAsync();

            return list.GetPaged(page, PAGE_SIZE);
        }

        public async Task<RentalResponse> RemoveStockAsync(RentalRequestBase request)
        {
            var removeRequest = (RemoveStockRequest)request;
            var stocks = await _dbContext.Stocks.Where(s => s.IdMovie == request.IdMovie).ToListAsync();

            if (stocks.Count < removeRequest.RemoveFromStock)
                return new RentalResponse { Success = false, Error = $"Sorry, there's just {stocks.Count} stock available to remove" };

            _dbContext.Stocks.RemoveRange(stocks.Take(removeRequest.RemoveFromStock));
            var result = await _dbContext.SaveChangesAsync();

            return result > 0
                ? new RentalResponse { Success = true }
                : new RentalResponse { Success = false, Error = "Error, the stocks can't be removed" };
        }

        public async Task<RentalResponse> RentalMovieAsync(RentalRequestBase request)
        {
            var rentalRequest = (RentalRequest)request;
            var movie = await _dbContext.Movies
                .Include(s => s.Stocks)
                .Where(m => m.IdMovie == rentalRequest.IdMovie)
                .SingleOrDefaultAsync();

            if (movie == null)
                return new RentalResponse { Success = false, Error = "Error, the movie selected doesn't exist" };

            var stock = movie.Stocks
                .FirstOrDefault(s => s.IsAvailable);

            if (stock == null)
                return new RentalResponse { Success = false, Error = "Error, there is not stock available" };

            rentalRequest.Payment = GetPriceForRentOrPurchase(rentalRequest.Status, movie);
            var transaction = _mapper.Map<HistoryTransaction>(rentalRequest);
            transaction.IdStock = stock.IdStock;
            stock.IsAvailable = false;

            await _dbContext.HistoryTransactions.AddAsync(transaction);
            var result = await _dbContext.SaveChangesAsync();
            _logger.LogInformation($"***** A movie was {rentalRequest.Status.ToString()} by {await _userManager.FindByIdAsync(transaction.IdUser)}, date: {DateTime.Now} *****");

            return result > 0 
                ? new RentalResponse { Success = true }
                : new RentalResponse { Success = false, Error = "Error, the transaction can't be processed" };
        }

        public async Task<RentalResponse> ReturnMovieAsync(RentalRequestBase request)
        {
            var returnRequest = (ReturnMovieRequest)request;
            var transaction = await _dbContext.HistoryTransactions
                .Include(s => s.Stock)
                .Include(m => m.Stock.Movie)
                .Where(h => h.IdTransaction == returnRequest.IdTransaction)
                .SingleOrDefaultAsync();

            transaction.Stock.IsAvailable = true;
            if (transaction.PenaltyDate < DateTime.Now)
                transaction.Payment += transaction.Stock.Movie.PenaltyMoney;

            transaction.ReturnDate = DateTime.Now;

            var result = await _dbContext.SaveChangesAsync();

            return result > 0
                ? new RentalResponse { Success = true }
                : new RentalResponse { Success = false, Error = "Error, the transaction can't be processed" };
        }

        public async Task<bool> UpdateMovieAsync(MovieRequestBase request)
        {
            var movieRequest = (UpdateMovieRequest)request;
            var movie = await _dbContext.Movies
                .Where(m => m.IdMovie == movieRequest.IdMovie)
                .SingleOrDefaultAsync();
            
            if (movie == null)
                return false;
            
            _logger.LogInformation($"***** A movie is going to be update ***** \n \r Movie values before update: Title = {movie.Title}, Rental Price = {movie.RentalPrice}, Sale price = {movie.SalePrice}");
            _mapper.Map(request, movie);

            var result = await _dbContext.SaveChangesAsync();
            await AddStocksAsync(movieRequest.IdMovie, movieRequest.StockQuantity);
            _logger.LogInformation($"Movie values after update: Title = {movie.Title}, Rental Price = {movie.RentalPrice}, Sale price = {movie.SalePrice}");
            return result > 0;
        }

        public async Task<LikeResponse> AddLikeAsync(LikeRequest request, ClaimsPrincipal claims)
        {
            var user = _userManager.GetUserId(claims);

            var validateLike = await _dbContext.Likes
                .Where(l => l.IdMovie == request.IdMovie && l.IdUser == user)
                .SingleOrDefaultAsync();

            if (validateLike != null)
                return new LikeResponse { Error = "You already like this movie " };

            var like = new Like
            {
                IdMovie = request.IdMovie,
                IdUser = user,
                Date = DateTime.Now
            };

            await _dbContext.Likes.AddAsync(like);
            var result = await _dbContext.SaveChangesAsync();

            return result > 0
                ? new LikeResponse { Success = true }
                : new LikeResponse { Error = "Error, the like can't be added" };
        }

        private async Task<int> AddStocksAsync(int idMovie, int stockQuantity)
        {
            var listStock = new List<Stock>();
            for (int i = 0; i < stockQuantity; i++)
            {
                listStock.Add(new Stock { IdMovie = idMovie, IsAvailable = true });
            }

            await _dbContext.Stocks.AddRangeAsync(listStock);
            return await _dbContext.SaveChangesAsync();
        }
        
        private decimal GetPriceForRentOrPurchase(StatusMovie status, Movie movie)
        {            
            return status == StatusMovie.Rented
                ? movie.RentalPrice
                : movie.SalePrice;
        }

        private Expression<Func<Movie, bool>> FilterMovies(Claim role, string name, bool isAvailable)
        {
            Expression<Func<Movie, bool>> filter = m => false;
            name ??= "";

            if (role == null || role.Value == UserRole.User.ToString())
            {
                filter = m => m.Stocks
                    .Where(s => s.IsAvailable).Count() > 0 && m.Title.Contains(name);
            }
            else
            {
                if (isAvailable)
                    filter = m => m.Title.Contains(name) &&
                                m.Stocks.Where(s => s.IsAvailable == isAvailable).Count() > 0;
                else
                    filter = m => m.Title.Contains(name) && m.Stocks
                        .Where(s => s.IsAvailable == isAvailable).Count() == m.Stocks.Count;
            }

            return filter;
        }
    }
}
