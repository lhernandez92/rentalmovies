﻿using System;

namespace RentalMovies.Application.Settings
{
    public class JwtSetting
    {
        public string Secret { get; set; }
        public TimeSpan TokenLifetime { get; set; }
    }
}
