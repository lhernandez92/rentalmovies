﻿namespace RentalMovies.Application.Settings
{
    public class SwaggerSetting
    {
        public string JsonRoute { get; set; }
        public string Description { get; set; }
        public string UIEndpoint { get; set; }
    }
}
