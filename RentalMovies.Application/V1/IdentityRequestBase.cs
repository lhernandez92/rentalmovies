﻿using System.ComponentModel.DataAnnotations;

namespace RentalMovies.Application.V1
{
    public abstract class IdentityRequestBase
    {
        [EmailAddress]
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
