﻿namespace RentalMovies.Application.V1
{
    public abstract class RentalRequestBase
    {
        public int IdMovie { get; set; }
        public string IdUser { get; set; }
    }
}
