﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentalMovies.Application.V1.Request
{
    public class RefreshTokenRequest : IdentityRequestBase
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}
