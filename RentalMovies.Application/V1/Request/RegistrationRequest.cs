﻿using RentalMovies.Domain.Enum;

namespace RentalMovies.Application.V1.Request
{
    public class RegistrationRequest : IdentityRequestBase
    {
        public UserRole Role { get; set; }
    }
}
