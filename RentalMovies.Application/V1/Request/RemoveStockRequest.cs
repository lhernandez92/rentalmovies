﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentalMovies.Application.V1.Request
{
    public class RemoveStockRequest : RentalRequestBase
    {
        public int RemoveFromStock { get; set; }
    }
}
