﻿using RentalMovies.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace RentalMovies.Application.V1.Request
{
    public class RentalRequest : RentalRequestBase
    {
        public int Days { get; set; }
        public StatusMovie Status { get; set; }
        public decimal Payment { get; set; }
    }
}
