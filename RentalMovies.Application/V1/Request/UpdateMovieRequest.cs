﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentalMovies.Application.V1.Request
{
    public class UpdateMovieRequest : MovieRequestBase
    {
        public int IdMovie { get; set; }
    }
}
