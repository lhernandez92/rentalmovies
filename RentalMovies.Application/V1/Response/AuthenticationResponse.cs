﻿namespace RentalMovies.Application.V1.Response
{
    public class AuthenticationResponse
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public bool Success { get; set; }
        public string Error { get; set; }
    }
}
