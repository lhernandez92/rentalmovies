﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentalMovies.Application.V1.Response
{
    public class LikeResponse
    {
        public bool Success { get; set; }
        public string Error { get; set; }
    }
}
