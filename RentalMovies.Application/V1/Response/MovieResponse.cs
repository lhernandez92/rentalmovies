﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentalMovies.Application.V1.Response
{
    public class MovieResponse
    {
        public int IdMovie { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public decimal RentalPrice { get; set; }
        public decimal SalePrice { get; set; }
        public decimal PenaltyMoney { get; set; }
        public bool IsAvailable { get; set; }
        public int StocksQuantity { get; set; }
        public int LikesQuantity { get; set; }
    }
}
