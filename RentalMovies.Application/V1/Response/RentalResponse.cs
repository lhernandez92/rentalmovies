﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentalMovies.Application.V1.Response
{
    public class RentalResponse
    {
        public bool Success { get; set; }
        public string Error { get; set; }
    }
}
