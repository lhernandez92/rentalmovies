﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RentalMovies.Data.Migrations
{
    public partial class AddedPaymentHistoryTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Payment",
                table: "HistoryTransactions",
                type: "decimal(4,2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Payment",
                table: "HistoryTransactions");
        }
    }
}
