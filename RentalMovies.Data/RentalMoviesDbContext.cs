﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RentalMovies.Domain.Entities;

namespace RentalMovies.Data
{
    public class RentalMoviesDbContext : IdentityDbContext
    {
        public RentalMoviesDbContext(DbContextOptions<RentalMoviesDbContext> options)
            : base(options)
        {

        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<HistoryTransaction> HistoryTransactions { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //Configuration using fluent API

            builder.Entity<Movie>(e =>
            {
                e.HasKey(e => e.IdMovie);
                e.Property(e => e.IdMovie)
                    .ValueGeneratedOnAdd();
                e.Property(e => e.Title).HasColumnType("varchar(120)").IsRequired();
                e.Property(e => e.Description).HasColumnType("varchar(500)").IsRequired(false);
                e.Property(e => e.Image).HasColumnType("varchar(1000)").IsRequired(false);
                e.Property(e => e.RentalPrice).HasColumnType("decimal(4,2)").IsRequired();
                e.Property(e => e.SalePrice).HasColumnType("decimal(4,2)").IsRequired();
                e.Property(e => e.PenaltyMoney).HasColumnType("decimal(4,2)").IsRequired();
            });

            builder.Entity<Stock>(e =>
            {
                e.HasKey(e => e.IdStock);
                e.HasIndex(e => e.IdMovie);
                e.Property(e => e.IdStock)
                    .HasColumnType("uniqueidentifier")
                    .ValueGeneratedOnAdd();
                e.Property(e => e.IsAvailable).HasColumnType("bit").IsRequired();

                e.HasOne(e => e.Movie)
                    .WithMany(f => f.Stocks)
                    .HasForeignKey(e => e.IdMovie);
            });

            builder.Entity<HistoryTransaction>(e =>
            {
                e.HasKey(e => e.IdTransaction);
                e.HasIndex(e => e.IdStock);
                e.HasIndex(e => e.IdUser);
                e.Property(e => e.IdUser).HasColumnType("nvarchar(450)");
                e.Property(e => e.IdStock).HasColumnType("uniqueidentifier").IsRequired();
                e.Property(e => e.RentalDate).HasColumnType("datetime").IsRequired();
                e.Property(e => e.PenaltyDate).HasColumnType("datetime").IsRequired(false);
                e.Property(e => e.ReturnDate).HasColumnType("datetime").IsRequired(false);                
                e.Property(e => e.Status).HasColumnType("int");
                e.Property(e => e.Payment).HasColumnType("decimal(4,2)");

                e.HasOne(e => e.Stock)
                    .WithMany(f => f.HistoryTransactions)
                    .HasForeignKey(e => e.IdStock);                

                e.HasOne(e => e.User)
                    .WithMany()
                    .HasForeignKey(e => e.IdUser);
            });

            builder.Entity<Like>(e =>
            {
                e.HasKey(e => new { e.IdMovie, e.IdUser });
                e.HasIndex(e => e.IdMovie);
                e.Property(e => e.IdMovie).HasColumnType("int").IsRequired();
                e.Property(e => e.IdUser).HasColumnType("nvarchar(450)").IsRequired();

                e.HasOne(e => e.Movie)
                    .WithMany(f => f.Likes)
                    .HasForeignKey(e => e.IdMovie);
            });

            builder.Entity<RefreshToken>(e =>
            {
                e.HasKey(e => e.Token);
                e.HasIndex(e => e.IdUser);
                e.Property(e => e.Token).HasColumnType("nvarchar(450)").ValueGeneratedOnAdd();
                e.Property(e => e.IdJwt).HasColumnType("nvarchar(max)");
                e.Property(e => e.CreationDate).HasColumnType("datetime").IsRequired();
                e.Property(e => e.ExpiryDate).HasColumnType("datetime").IsRequired();
                e.Property(e => e.Used).HasColumnType("bit").IsRequired();
                e.Property(e => e.Invalidated).HasColumnType("bit").IsRequired();
                e.Property(e => e.IdUser).HasColumnType("nvarchar(450)");

                e.HasOne(e => e.User)
                    .WithMany()
                    .HasForeignKey(e => e.IdUser);
            });
        }
    }
}
