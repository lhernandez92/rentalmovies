﻿using Microsoft.AspNetCore.Identity;
using RentalMovies.Domain.Enum;
using System;

namespace RentalMovies.Domain.Entities
{
    public class HistoryTransaction
    {
        public int IdTransaction { get; set; }
        public string IdUser { get; set; }
        public Guid IdStock { get; set; }
        public DateTime RentalDate { get; set; }
        public DateTime? PenaltyDate { get; set; }
        public DateTime? ReturnDate { get; set; }        
        public StatusMovie Status { get; set; }
        public decimal Payment { get; set; }

        //Entities relations
        public virtual Stock Stock { get; set; }
        public virtual IdentityUser User { get; set; }
    }
}
