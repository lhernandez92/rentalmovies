﻿using Microsoft.AspNetCore.Identity;
using System;

namespace RentalMovies.Domain.Entities
{
    public class Like
    {
        public int IdMovie { get; set; }
        public string IdUser { get; set; }
        public DateTime Date { get; set; }

        //Entities relations
        public virtual Movie Movie { get; set; }
    }
}
