﻿using System.Collections.Generic;

namespace RentalMovies.Domain.Entities
{
    public class Movie
    {
        public Movie()
        {
            Stocks = new HashSet<Stock>();
            Likes = new HashSet<Like>();
        }

        public int IdMovie { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public decimal RentalPrice { get; set; }
        public decimal SalePrice { get; set; }
        public decimal PenaltyMoney { get; set; }

        //Entities relations
        public virtual ICollection<Stock> Stocks { get; set; }
        public virtual ICollection<Like> Likes { get; set; }
    }
}
