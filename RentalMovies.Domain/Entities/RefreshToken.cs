﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace RentalMovies.Domain.Entities
{
    public class RefreshToken
    {
        public string Token { get; set; }
        public string IdJwt { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool Used { get; set; }
        public bool Invalidated { get; set; }
        public string IdUser { get; set; }

        //Entities relations
        public virtual IdentityUser User { get; set; }
    }
}
