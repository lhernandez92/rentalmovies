﻿using System;
using System.Collections.Generic;

namespace RentalMovies.Domain.Entities
{
    public class Stock
    {
        public Stock()
        {
            HistoryTransactions = new HashSet<HistoryTransaction>();
        }

        public Guid IdStock { get; set; }
        public int IdMovie { get; set; }
        public bool IsAvailable { get; set; }

        //Entities relations
        public virtual Movie Movie { get; set; }
        public virtual ICollection<HistoryTransaction> HistoryTransactions { get; set; }
    }
}
