﻿namespace RentalMovies.Domain.Enum
{
    public enum StatusMovie
    {
        Rented = 1,
        Purchased = 2
    }
}
