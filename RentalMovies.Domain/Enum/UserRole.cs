﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentalMovies.Domain.Enum
{
    public enum UserRole
    {
        Admin = 1,
        User = 2
    }
}
